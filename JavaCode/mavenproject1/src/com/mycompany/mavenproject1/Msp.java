/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.io.IOException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
/**
 *
 * @author 10
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost", allowCredentials = "true")
public class Msp {
    
    @GetMapping("/test") // Menambahkan pemetaan untuk endpoint /test
    public String testEndpoint() {
        return "Ini adalah halaman test";
    }

    @PostMapping("/receiveData")
    public String receiveData(@RequestBody String data) {
        // Di sini Anda dapat melakukan apa pun dengan data yang diterima
        System.out.println("Data yang diterima: " + data);

        // Menjalankan skrip batch tanpa menampilkan jendela cmd
        try {
            ProcessBuilder builder = new ProcessBuilder("cmd", "/c", "C:\\Users\\10\\Documents\\NetBeansProjects\\mavenproject1\\target\\trigger.bat");
            builder.start();
            return "Skrip batch berhasil dijalankan tanpa tampilan cmd";
        } catch (IOException e) {
            e.printStackTrace();
            return "Terjadi kesalahan saat menjalankan skrip batch";
        }
    }
}

