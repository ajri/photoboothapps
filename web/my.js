document.addEventListener("DOMContentLoaded", function() {
    // Event listener untuk tombol Start
    document.getElementById("startBtn").addEventListener("click", function() {
        
        // Mendapatkan nilai 'total' dan 'email' dari input pengguna
        var total = 20000;
        var email = "contoh@gmail.com";

        // Mendapatkan nilai 'identity' dan 'alamat' dari query string
        var urlParams = new URLSearchParams(window.location.search);
        var identity = urlParams.get('identity');
        var alamat = urlParams.get('alamat');

        // data yang akan dikirim ke midtrans setelah tombol ditekan
        sendData(total, email, identity, alamat);
    });
});

async function sendData(total, email, identity, alamat) {
    // Membuat objek data dengan nilai 'total', 'email', 'identity', dan 'alamat'
    var data = {
        total: total,
        email: email,
        identity: identity,
        alamat: alamat
    };

    try {
        const response = await fetch('place.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const token = await response.text();
        console.log('Snap Token:', token);

        // Pemanggilan fungsi untuk memicu pop-up pembayaran
        showPaymentPopup(token);
    } catch (error) {
        console.error('Error:', error);
    }
}


function sendDataToBackend() {
    // Data yang akan dikirim ke backend
    var dataToSend = {
        key1: 'OKE',
        key2: 'BERHASIL'
    };

    // URL endpoint backend
    var endpointUrl = 'http://localhost:8080/api/receiveData'; // Sesuaikan dengan URL endpoint Anda

    // Konfigurasi permintaan
    var requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    };

    // Mengirim permintaan ke backend
    fetch(endpointUrl, requestOptions)
        .then(response => response.text())
        .then(result => {
            console.log('Respon dari server:', result);
        })
        .catch(error => console.error('Terjadi kesalahan:', error));
}

// Fungsi untuk memicu pop-up pembayaran dan memulai sesi cetak di dslrBooth setelah transaksi berhasil
function showPaymentPopup(token) {
    window.snap.pay(token, {
        onSuccess: function(result){
            // Transaksi berhasil, memulai sesi cetak di dslrBooth
            sendDataToBackend();
            startPrintSession();
            console.log(result);
        },
        onPending: function(result){
            console.log(result);
        },
        onError: function(result){
            console.log(result);
        },
        onClose: function(){
            alert('You closed the popup without finishing the payment');
        }
    });
}

// Fungsi untuk memulai sesi cetak di dslrBooth
function startPrintSession() {
    var password = "pyPinsDdbvg2OZMw"; // Ganti dengan password yang dihasilkan oleh dslrBooth
    var apiUrl = "http://localhost:1500/api/start?mode=print&password=" + password;

    fetch(apiUrl)
        .then(function(response) {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(function(data) {
            if (data.IsSuccessful) {
                // Sesi cetak berhasil dimulai
                //alert("Print session started successfully!");
            } else {
                // Terjadi kesalahan saat memulai sesi cetak
                //alert("Failed to start print session. Error: " + data.ErrorMessage);
            }
        })
        .catch(function(error) {
            // Handle error
            console.error('There has been a problem with your fetch operation:', error);
        });
}

    
